import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessagesModule } from './modules/messages/messages.module';
import { ConfigModule } from '@nestjs/config';
import { AmqpModule } from './microservices/amqp.module';
@Module({
  imports: [
    MessagesModule,
    AmqpModule,
    ConfigModule.forRoot({
      envFilePath: '.env.development',
      ignoreEnvFile: process.env.NODE_ENV === 'development' ? false : true,
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
