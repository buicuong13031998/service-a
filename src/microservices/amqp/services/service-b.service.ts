import { Injectable } from '@nestjs/common';
import { AmqpProducer } from '../../amqp.producer';
@Injectable()
export class ServiceBService {
  constructor(private readonly _amqpProducer: AmqpProducer) {}

  public async sendMessage(message: string): Promise<any> {
    this._amqpProducer.sendMessage(message);
  }
  public async sendMessageRpc(message: string): Promise<any> {
    this._amqpProducer.sendMessageRpc(message);
  }
}
