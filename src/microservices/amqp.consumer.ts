/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  AmqpConnection,
  Nack,
  RabbitRPC,
  RabbitSubscribe,
} from '@golevelup/nestjs-rabbitmq';
import { AmqpProducer } from './amqp.producer';

@Injectable()
export class AmqpConsumer {
  constructor() {}

  //* receive message from RingCentral
  @RabbitSubscribe({
    exchange: 'team_name.project_name',
    routingKey: 'message.cmd.receive.service-b.service-a',
    queue: 'team_name.project_name-message.cmd.receive.service-b.service-a',
    queueOptions: {
      deadLetterExchange: 'team_name.dlx_project_name',
      deadLetterRoutingKey: `dlx.log`,
    },
  })
  public async receivedInboundMessage(message: any) {
    // const messageExisted = await this._messagesRepository.findOne({
    //   where: {
    //     exId: message.exId,
    //     status: EntityStatus.ACTIVE,
    //   },
    // });
    // if (!messageExisted) {
    //   const data = await this._inboundMessageQueueService.inboundMessageJobs(
    //     message,
    //   );
    //   await this._realTimeService.receivedMessageBackend([data]);
    // }
  }

  //* update message from RingCentral

  @RabbitRPC({
    exchange: 'team_name.project_name',
    routingKey: 'schedule_message.cmd.run.scheduler.backend',
    queue: 'team_name.project_name-schedule_message.cmd.run.scheduler.backend',
    queueOptions: {
      deadLetterExchange: 'phpswteam.dlx_php_text_message',
      deadLetterRoutingKey: `dlx.log`,
    },
  })
  public async runScheduleMessage(data) {
    // try {
    //   return await this._scheduleMessageService.runScheduleMessage(
    //     data.request.scheduleMessageId,
    //   );
    // } catch (error) {
    //   return new Nack();
    // }
  }
}
