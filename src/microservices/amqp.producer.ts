import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
@Injectable()
export class AmqpProducer {
  constructor(private readonly _amqpConnection: AmqpConnection) {}

  public async sendMessage(message) {
    this._amqpConnection.publish(
      'team_name.project_name',
      'message.cmd.send.service_a.service_b',
      { message },
    );
  }
  //* SCHEDULE MESSAGE
  public async sendMessageRpc(message) {
    try {
      const data = await this._amqpConnection.request<any>({
        exchange: 'team_name.project_name',
        routingKey: 'message.cmd.send-rpc.service_a.service_b',
        payload: {
          request: message,
        },
        timeout: 10000,
      });
      if (data) {
        return data;
      }
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }
}
