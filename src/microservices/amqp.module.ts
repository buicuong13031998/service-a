/* eslint-disable @typescript-eslint/no-var-requires */
import { AmqpConsumer } from './amqp.consumer';
import { AmqpProducer } from './amqp.producer';
import { Module } from '@nestjs/common';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

import { ConfigModule } from '@nestjs/config';
import { rabbitMqConfigAsync } from 'src/common/config/rabbitmq.config';

require('dotenv').config();
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env.development',
      ignoreEnvFile: process.env.NODE_ENV === 'development' ? false : true,
    }),
    RabbitMQModule.forRootAsync(RabbitMQModule, rabbitMqConfigAsync),
  ],
  providers: [
    AmqpProducer,
    AmqpConsumer,
    // RealtimeService,
  ],
  exports: [
    // RealtimeService,
    AmqpProducer,
  ],
})
export class AmqpModule {}
