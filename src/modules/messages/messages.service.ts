import { Injectable } from '@nestjs/common';
import { ServiceBService } from '../../microservices/amqp/services/service-b.service';

@Injectable()
export class MessagesService {
  constructor(private readonly serviceB: ServiceBService) {}
  sendMessage() {
    return this.serviceB.sendMessage('Hi!!!');
  }
}
